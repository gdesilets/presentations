# CBX4 Dev draft proposition

#### Inspiration

En suivant le ADR(Action Domain Responder) pattern qui offre une alternative au MVC pattern.

Pour faire la comparaison :

Model      <--> Domain

View       <--> Responder

Controller <--> Action

####  Attention !

En comparaison au Controller dans le MVC pattern l'action dans ADR ne contient pas plusieurs methodes pour differentes actions. Dans ADR chaque actions est representees par un et un seul controller.

===

## Suggestion !

On n'a pas besoin d'un Controller par model.

On devrait creer un nouveau controller quand :

- On penses a ecrire une non RESTful action dans un controller.
  Ex : `employee/21/banish` ou banish est le verbe.

  _Ceci est qu'un exemple, dans notre situation nous laissons banish dans employee, car un banished employee n'est pas une sub-instance d'employee, si le cas ce présente il serait approprié de l'extraire dans son propre controller etc._

- Il y a quelque chose qu'on peux nommer et qui est important donc peut etre une ressource séparé.

Essayer le plus possible dans un controller de ce tenir au 5 methodes de rails :

- index
- show
- create
- update
- delete

Deplacer de la logic dans des services objects pour reduire la job du model.

