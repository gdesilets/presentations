# Making rich Domain with Rails & Active Record !
##### ...with ERD

## Disclaimer
J'aimerais garder le modèle de domaine et la persistance découplé, comme un vrai DataMapper ORM permet (par exemple, Hibernate, Doctrine).

L'approches est un compromis pour ne pas changer au complet les methodes actuelles, tout en atteignant un certain découplage.

## Data Mapper ?
Une couche de mappers qui déplace les données entre les objets et une database tout en les gardant indépendants les uns des autres et le mappeur lui-meme.
Sa sépare les objets in-memory de la database. Les objets in-memory n'ont meme pas besoins de savoir qu'il existe une database.

## Repository ?
Un repository sert d'intermédiaire entre le domaine et le data mapping layer, aggissant comme une collection d'objets de domaines in-memory.
Pour plus voir Martin Fowler P of EAA (Patterns of Enterprise Application Architecture) page 322.

## Problematiques avec Active Record

- Le model hérite d'active record, donc nous avons besoin d'active record pour exécuter les tests

- Une instance de la classe est responsable de se sauvegarder et ce modifier elle-meme. Ce qui rend le `mocking` et le `stubbing` plus difficile

- Chaque instance expose des methodes `low-level` comme `update_attributes`. Elles donnent trop de liberté pour changer le internal state des objets . Ca peut corrompte le data et c'est pourquoi souvent on remarque que `update_attributes` est utilisé a plusieurs endroits.(Dans notre cas, 87 matches across 33 files)

- Les associations `Has many` nous laisse contourner un regroupement globale.

- Chaque instance est reponsable pour ce valider elle-meme. C'est plus difficile a tester. De plus ca rend la validations plus dur a composer.

- _L'utilisation de méthode static pour selectionner le data rend le testing plus compliqué._

**   
  _Le probleme avec les methodes static, c'est quelles ont du code procédural. Faire un test unitaire sur du code procédural n'est pas chose simple. Les test unitaire assument que nous pouvons instancier une partie de l'application de facon isolé. Durant l'instanciation on branche ensemble les dependances avec des mocks qui remplacent les vrais dépendences._

  _Avec du procédural il n'y a rien a brancher ensemble puisque il n'y a pas d'objet, le code et le data sont separés._

  _Une autre facon de le voir est que les tests unitaires on besoins de coutures, la couture est ou l'on previent l'exécution du cheminement normal du code et c'est comment on atteint l'isolation de la class pour les tests.
  Oui les methodes static sont facile a caller, mais si la methode static en call une autre il n'y aucun moyen d'overider la dépendance en les calls._

## SOLUTION

Suivant le motto de Rich Hickey, le createur du language Clojure, de diviser les choses, la meilleur solution que je suggere est de separer chaque classe d'active record(well not at 100%) en 3 classes différentes. D'ou le nom Domaine au lieu de modele.

### Classes

- Entity
- Data Object
- Repository

L'idée principale est que chaqu'une des entitées, quand elles est instantient,recoivent un `data object`. L'entitée délegue ses `fields access` au `data object`. Le `data object` n'a pas besoin d'etre un objet Active Record. On peu toujours toujours lui fournir un `stub` ou une `OpenStruct` a la place. Puiseque l'entitée est un P.O.R.O, il ne sait pas comment se sauvegarder|modifier|valider elle-meme. Elle ne sais pas non plus comment aller ce fetcher elle-meme depuis la database. Un `reposirtory` est responsable d'aller chercher le `data object` depuis la database et de contruire les entitées. Il est aussi responsable de la creer et mettre a jour les entitées.
Pour en finir avec ses reponsabilitées le `repository` doit savoir comment mapper le `data object` aux entitées. Un `registry` de tous les `data objects` et leur entitée correspondante est créé pour executer cette tache.

### EXEMPLE

#### Schema

```Ruby
create_table "business_units", :force => true do |t|
  t.string   "name"
  t.string   "address"
  t.datetime "created_at", :null => false
  t.datetime "updated_at", :null => false
end

create_table "employees", :force => true do |t|
  t.string   "name"
  t.string   "age"
  t.integer  "business_unit_id"
  t.datetime "created_at", :null => false
  t.datetime "updated_at", :null => false
end
```
#### Data Objects

```Ruby
class BusinessUnitData < ActiveRecord::Base
  self.table_name = "business_untis"

  attr_accessible :name, :address

  validates :name, presence: true
  has_many :employees, class_name: "EmployeeData", foreign_key: "business_unit_id"
end

class EmployeeData < ActiveRecord::Base
  self.table_name = "employees"

  attr_accessible :name, :age

  validates :age, presence: true
  validates :name, presence: true, length: { in: 1..35 }
end
```

#### DOMAIN OBJECTS... Yay ! PORO !

```Ruby
class BusinessUnit
  include Edr::Model

  # Delegates id, id=, name, name=, address to the data object
  fields :id, :name, :address

   # ...
end

class Employee
  include Edr::Model
  fields :id, :name, :desciption
end
```

#### MAP DOMAIN OBJECTS TO DATA OBJECTS

```Ruby
Edr::Registry.define do
  map BusinessUnit, BusinessUnitData
  map Employee, EmployeeData
end
```
#### REPOSITORY
Comme BusinessUnit et Employee forme un regroupement, on a une référence a employee seulement via sa business unit. 

Par conséquent, nous avous seulement besoin d'implementer un seul repository:
```Ruby
module BusinessRepository
  extend Edr::AR::Repository
  set_model_class BusinessUnit 

  def self.find_by_address address
    where(address: address)
  end
end
```

#### TESTS !

```Ruby
describe "Persisting objects" do
  example do
    bu = BusinessUnit.new name: 'Coco', address: '1234 terrace evergreen'

   BusinessUnitRepository.persist bu

    expect(bu.id).to be_present
    expect(bu.name).to eql('Coco')
    expect(bu.address).to eql('1234 terrace evergreen')
  end

  it "persists an aggregate with children" do
    bu = BusinessUnit.new name: 'Coco', address: '1234 terrace evergreen'
    bu.add_employee name: 'Kyle', age: 23

    BusinessUnitRepository.persist bu

    from_db = BusinessUnitRepository.find(bu.id)
    expect(from_db.employees.first.name).to eql('Kyle')
  end
end

describe "Selecting models" do
  let!(:data){ BusinessUnitData.create! name: 'John Doe cie', address: 'shiny address' }

  example do
    bu = DubisnessUnitRepository.find_by_address 'shiny new address'
    expect(bu.first.id).to eql(data.id)
  end

    it "raises an exception when cannot find cannot object" do
    { BusinessUnitRepository.find 999 }.to raise_error
  end
end
```

#### ASSOCIATIONS

Un aspect important quand on fait du domaine riche est comment les associations entre une racine globale et ses enfants sont gérés ? Comment on accedes au employees ? Il y a deux solutions possible. La premiere est d'utilier une `asspciation` et un `wrap` helper methodes.

```Ruby
class BusinessUnit
  include Edr::Model

  # Delegates id, id=, name, name=, address to the data object
  fields :id, :name, :address
  wrap_associations :employees
   # ...

   # returns the data object’s association (has_many)
  def add_employee attrs
    wrap association(:employees).new(attrs)
  end
end
```

```Ruby
class BusinessUnit
  include Edr::Model

  # Delegates id, id=, name, name=, address to the data object
  fields :id, :name, :address
  
   # ...

   # returns the data object’s association (has_many)
  def add_employee attrs
    repository.create_employee self, attrs
  end
end
```

#### VALIDATIONS

Puisque que les data objects sont caches, et ne sont pas suposer etre accessible direcment par le client, nous avons besoins la facon nous validations les donnees.

```Ruby
Class DataValidator
  def self.validate model
    data = model._data
    data.valid?
    data.errors.full_messages
  end
end
```
##### Exemple d'utilisation des validations

```Ruby
bu_data = BusinessUnitData.create! name: 'Coco', address: '123 fake street'
bu = BusinessUnit.new(bu_data)
bu.name = "Bleh"
expect(Edr::AR::DataValidator.validate(bu)).to be_present
```
### Ce que nous avons maintenant !

- Nous avons maintenant un niveau d'abstraction avec la logic du domaine

- [ERD workflow](https://www.lucidchart.com/invitations/accept/f0aea0c3-18ea-4ddc-b524-a8f14b4d2922)

- La logic de persistence a ete extract dans un repository. Avoir un objet separer est benefique pour plusieurs raisons. Par exemple, ca simplifie le testings, car il peut etre mock ou faked plus facilement.

- L'instance de BusinessUnit et Employee ne sont plus responsable de sauvegarder ou se mettre a jour eux-meme. La seule facon de faire est d'utiliser les methodes specifiques au domaine.

- Les methodes low-level comme `update_attributes!` ne sont plus exposes.

- Il n'y a plus de `has_many` global, cela assure une regroupement local avec des limites

- Avoir les validations separees permet une meilleur composition et simplifie le testing.

Add a comment to this line
### Wrap up !

L'approche suggere est simple, mais apporte de la vrais valeur quand il temps de s'attaquer a des domaines complexes. EDR ce jumelle bien avec une legacy application ou du legacy code, car rien n'a besoin d'etre re-ecris ou redesigned from scratch. Simplement utiliser les models Active Record existant comme `DataClass` quand on ajoute une nouvelle fonctionalite.


### Small hint for the futur (-; 

soli**D** principle.
